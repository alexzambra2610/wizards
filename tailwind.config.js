/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/*.{html,js}"],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1512px',
    },
    colors: {
      
      'purple': '#7e5bef',
      'pink': '#ff49db',
      'orange': '#ff7849',
   
      'yellow': '#ffc82c',
      'gray-dark': '#273444',
      'gray': '#8492a6',
      'gray-light': '#d3dce6',
      'white': '#ffff',
      'black': '#292c30'
    },
    fontFamily: {
      sans: ['Open Sans', 'sans-serif'],
      jakarta: ['Plus Jakarta Sans', 'arial'],
    },
    extend: {},
  },
  plugins: [],
}

